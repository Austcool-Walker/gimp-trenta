# Maintainer: Levente Polyak <anthraxx[at]archlinux[dot]org>
# Contributor: Daniel Isenmann <daniel@archlinux.org>

pkgname=gimp-trenta
_pkgname=gimp
pkgver=2.8.10
_extra_ver=2.8.10
pkgrel=1
pkgdesc='GNU Image Manipulation Program'
url='https://www.gimp.org/'
arch=('x86_64')
license=('GPL' 'LGPL')
depends=('babl' 'dbus-glib' 'desktop-file-utils' 'gegl' 'glib-networking' 'hicolor-icon-theme'
         'jasper' 'lcms2' 'libheif' 'libexif' 'libgudev' 'libmng' 'libmypaint' 'librsvg' 'libwebp'
         'libwmf' 'libxmu' 'libxpm' 'mypaint-brushes' 'openexr' 'poppler-data' 'pygtk' 'gegl02')
makedepends=('alsa-lib' 'curl' 'ghostscript' 'gtk-doc' 'intltool' 'iso-codes' 'poppler-glib')
optdepends=('gutenprint: for sophisticated printing only as gimp has built-in cups print support'
            'poppler-glib: for pdf support'
            'alsa-lib: for MIDI event controller module'
            'curl: for URI support'
            'ghostscript: for postscript support')
conflicts=('gimp-plugin-wavelet-decompose' 'gimp')
replaces=('gimp-plugin-wavelet-decompose' 'gimp')
provides=('gimp' 'gimp-trenta')
source=("http://ppa.launchpad.net/trentaos-team/ppa/ubuntu/pool/main/g/gimp/gimp_${pkgver}.orig.tar.bz2"
  "http://ppa.launchpad.net/trentaos-team/ppa/ubuntu/pool/main/g/gimp/gimp_${_extra_ver}-3trenta1.debian.tar.gz")
sha256sums=('e7fd8b19f989138d826003c75f56bd5b6f136eef597e86e3978ede0bba470ae6'
            'bd80df0e76b535b69d618252c6d4efcb8f2d59e278bb59f7aedaf85b1fe0d648')

prepare() {
  cd ${_pkgname}-${pkgver}
      for i in $(grep -v '#' ${srcdir}/debian/patches/series); do
        patch -p1 -i "${srcdir}/debian/patches/${i}"
    done
  _mypaintver=$(echo /usr/lib/libmypaint-*.so | grep -o -E '\-[0-9]+(\.[0-9]+)*' | head -1)
  sed -i "s|\\(libmypaint\\)\\( >= libmypaint_required_version\\)|\\1${_mypaintver}\\2|g" configure.ac
  autoreconf -vi
}

build() {
pkg-config --exists --print-errors "OpenEXR >= 1.6.1"
  cd ${_pkgname}-${pkgver}
  PYTHON=/usr/bin/python2 ./configure \
    --prefix=/usr \
	--enable-default-binary \
	--enable-gtk-doc \
	--with-lcms=lcms2 \
	--sysconfdir=/etc \
    --libexecdir=/usr/bin \
    --enable-mp \
    --enable-gimp-console \
    --enable-python \
    --disable-static \
    --disable-gtk-doc \
    --with-bug-report-url='https://bugs.archlinux.org/?string=gimp' \
    --with-openexr \
    --without-aa
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
  make
}

package() {
  cd ${_pkgname}-${pkgver}
  make DESTDIR="${pkgdir}" install
  install -D -m644 "${BUILDDIR}/linux.gpl" "${pkgdir}/usr/share/gimp/2.0/palettes/Linux.gpl"

  rm "${pkgdir}/usr/share/man/man1/gimp-console.1"
  ln -s gimp-console-${pkgver%.*}.1.gz "${pkgdir}/usr/share/man/man1/gimp-console.1.gz"
  ln -s gimptool-2.0 "${pkgdir}/usr/bin/gimptool"
  ln -sf gimptool-2.0.1.gz "${pkgdir}/usr/share/man/man1/gimptool.1.gz"
}

# vim: ts=2 sw=2 et:
